//
//  ViewController.m
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import "ViewController.h"

#import "UMUserData.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.placesView.placesCollection.collectionViewLayout invalidateLayout];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPlaceList) name:@"kRefreshPlacesList" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showFoecastForCurrent) name:@"kShowForecast" object:nil];
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self showPlaces];
}

- (void) refreshPlaceList {
    [self.placesView.placesCollection reloadData];
    [self showPlaces];
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.placesView.placesCollection.collectionViewLayout invalidateLayout];
}

- (IBAction)addPlaceBtnPressed:(UIButton *)sender {
    [self showMap];
}

- (IBAction)showPlacesBtnPressed:(UIButton *)sender {
    [self showPlaces];
}

- (IBAction)showHelpBtnPressed:(UIButton *)sender {
    [self showHelp];
}

- (void) showHelp {
    self.helpView.hidden = NO;
    self.placesView.hidden = YES;
    self.forecastView.hidden = YES;
    self.mapView.hidden = YES;
}

- (void) showMap {
    self.helpView.hidden = YES;
    self.placesView.hidden = YES;
    self.forecastView.hidden = YES;
    self.mapView.hidden = NO;
}

- (void) showPlaces {
    self.helpView.hidden = YES;
    self.placesView.hidden = NO;
    self.forecastView.hidden = YES;
    self.mapView.hidden = YES;
}

- (void) showForecasts {
    self.helpView.hidden = YES;
    self.placesView.hidden = YES;
    self.forecastView.hidden = NO;
    self.mapView.hidden = YES;
}

- (void)showFoecastForCurrent {
    [self showForecasts];
    [self.forecastView loadCuttentPlaceData];
}
@end
