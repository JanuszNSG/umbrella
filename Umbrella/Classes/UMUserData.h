//
//  UMUserData.h
//  Umbrella
//
//  Created by Janusz Bień on 21.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UMUserData : NSObject

extern NSString *const kUmbrellaUserDefPlacesStore;
extern NSString *const kUmbrellaUserDefHelpShowed;
extern NSString *const kUmbrellaUserDefUnitSettings;

@property (nonatomic, retain) NSMutableArray *userPlaces;
@property (readwrite) NSUInteger currentPlace;

+ (instancetype)sharedInstance;

- (void) getLocalData;
- (NSUInteger) numberOfLocalData;
- (void) addPlaceAtLat: (CGFloat) lat Lon: (CGFloat) lon;
- (void) deletePlaceAtIndex: (NSUInteger) index;

@end
