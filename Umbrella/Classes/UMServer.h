//
//  UMServer.h
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UMServer : NSObject

@property (nonatomic, retain) NSDictionary *weatherData;
@property (readwrite) BOOL weatherWithForecast;

- (void)getCurrentWeatherForLat: (CGFloat) lat Lon: (CGFloat) lon;
- (void)getForecastedWeatherForLat: (CGFloat) lat Lon: (CGFloat) lon;

@end
