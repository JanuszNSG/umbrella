//
//  UMServer.m
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import "UMServer.h"

NSString* const kOpenWeatherDomain = @"api.openweathermap.org";
NSString* const kOpenWeatherData = @"/data/2.5/";
NSString* const kOpenWeatherCurrent = @"weather";
NSString* const kOpenWeatherForecast = @"forecast";

NSString* const kOpenWeatherAPIKey = @"c6e381d8c7ff98f0fee43775817cf6ad";

@interface UMServer()

@property (nonatomic, retain) NSURLRequest *reques;
@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, retain) NSError *error;
@property (nonatomic, retain) NSData *data;
@property (nonatomic, retain) NSURLSession *session;

@end

@implementation UMServer

- (instancetype) init {
    if (self = [super init]){
        _reques = nil;
        _response = nil;
        _error = nil;
        _data = nil;
        _weatherData = nil;
        _session = [NSURLSession sharedSession];
        [self getWeatherForLat: 51.0f Lon:21.0f withForecast:YES];
        [self getWeatherForLat: 51.0f Lon:21.0f withForecast:NO];
    }
    return self;
}

- (NSURL *)URLForLat: (CGFloat) lat Lon: (CGFloat) lon withForecast: (BOOL) forecast {
    NSString *command = @"";
    if (forecast) {
        command = kOpenWeatherForecast;
    } else {
        command = kOpenWeatherCurrent;
    }
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@%@%@", kOpenWeatherDomain, kOpenWeatherData, command]];
    NSURLComponents *components = [[NSURLComponents alloc] initWithURL:url resolvingAgainstBaseURL:NO];
    NSArray *query = @[
                       [NSURLQueryItem queryItemWithName:@"appid" value:kOpenWeatherAPIKey],
                       [NSURLQueryItem queryItemWithName:@"lat" value:[NSString stringWithFormat:@"%f", lat]],
                       [NSURLQueryItem queryItemWithName:@"lon" value:[NSString stringWithFormat:@"%f", lon]]
                       ];
    [components setQueryItems:query];
    NSLog(@"URL: %@", components.URL);
    return [components URL];
}

- (void) getWeatherForLat: (CGFloat) lat Lon: (CGFloat) lon withForecast: (BOOL) forecast {
    if (self.session) {
        [[self.session dataTaskWithURL:[self URLForLat: lat Lon:lon withForecast:forecast]
                completionHandler:^(NSData *data,
                                    NSURLResponse *response,
                                    NSError *error) {
                    if (data) {
                        if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                            NSUInteger statusCode = [(NSHTTPURLResponse *) response statusCode];
                            if (statusCode!= 200) {
                                NSLog(@"HTTP error %lu...", (unsigned long)statusCode);
                                return;
                            }
                        } else {
                            NSLog(@"Response error...");
                            return;
                        }
                    } else {
                        NSLog(@"Connection error...");
                        return;
                    }
                    NSError *parseError = nil;
                    self.weatherData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                    if (!self.weatherData) {
                        NSLog(@"Parse error...");
                        return;
                    }
                    NSLog(@"\n%@\n", self.weatherData);
                    self.weatherWithForecast = forecast;
                }] resume];
    }
}

- (void) getCurrentWeatherForLat: (CGFloat) lat Lon: (CGFloat) lon {
    [self getWeatherForLat:lat Lon:lon withForecast:NO];
}

- (void) getForecastedWeatherForLat: (CGFloat) lat Lon: (CGFloat) lon {
    [self getWeatherForLat:lat Lon:lon withForecast:YES];
}

@end
