//
//  UMForecastCell.m
//  Umbrella
//
//  Created by Janusz Bień on 21.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import "UMForecastCell.h"

@implementation UMForecastCell

- (void) fillWithData: (NSDictionary *) data atIndex: (NSUInteger) index {
    NSString *placeIconName = @"";
    NSString *placeDay = @"";
    NSString *placeDesc = @"";
    NSString *placeTemp = @"";
    NSString *placeHuminidy = @"";
    NSString *placeWind = @"";
    if (index == 0) {
        placeDay = @"+1 day";
    } else {
        placeDay = [NSString stringWithFormat:@"+%lu days", (unsigned long)index + 1];
    }
    if (data[@"weather"]) {
        placeDesc = data[@"weather"][0][@"description"];
        placeIconName = data[@"weather"][0][@"icon"];
    }
    if (data[@"main"]) {
        placeTemp = [NSString stringWithFormat:@"%0.0f°C", [data[@"main"][@"temp"] floatValue]];
        placeHuminidy = [NSString stringWithFormat:@"%0.0f%%", [data[@"main"][@"humidity"] floatValue]];
    }
    if (data[@"wind"]) {
        placeWind = [NSString stringWithFormat:@"%0.0fm/s", [data[@"wind"][@"speed"] floatValue]];
    }
    self.weatherData = [[UMWeatherData alloc] init];
    if (placeIconName.length>0) {
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData *imageData = [[NSData alloc] initWithContentsOfURL: [self.weatherData URLForIcon:placeIconName]];
            if ( imageData != nil ) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.placeIcon.image = [UIImage imageWithData: imageData];
                    self.placeIcon.hidden = NO;
                });
            }
        });
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.placeDay.text = placeDay;
        self.placeDesc.text = placeDesc;
        self.placeTemp.text = placeTemp;
        self.placeHuminidy.text = placeHuminidy;
        self.placeWind.text = placeWind;
    });
}
@end
