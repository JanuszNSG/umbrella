//
//  UMPlaceCell.h
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMWeatherData.h"

@protocol UMPlaceCellDelegate <NSObject>
@required
- (void)showFoecastFor: (NSUInteger) index;
@end

@interface UMPlaceCell : UICollectionViewCell <UIGestureRecognizerDelegate, UMWeatherDataDelegate>

@property(nonatomic, weak) IBOutlet UILabel *placeName;
@property(nonatomic, weak) IBOutlet UILabel *placeDesc;
@property(nonatomic, weak) IBOutlet UILabel *placeTemp;
@property(nonatomic, weak) IBOutlet UILabel *placeHuminidy;
@property(nonatomic, weak) IBOutlet UILabel *placeWind;

@property(nonatomic, weak) IBOutlet UIImageView *placeIcon;

@property(nonatomic, weak) IBOutlet UIButton *placeDeleteBtn;

@property(nonatomic, weak) IBOutlet UIActivityIndicatorView *placeBuzyIndicator;

@property(nonatomic, strong) UMWeatherData *weatherData;

@property (readwrite) BOOL readyToDelete;

@property (nonatomic, weak) id <UMPlaceCellDelegate> delegate;

- (void) fillWithData: (NSDictionary *) data;

@end
