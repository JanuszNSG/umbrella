//
//  UMUserData.m
//  Umbrella
//
//  Created by Janusz Bień on 21.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import "UMUserData.h"

@implementation UMUserData

NSString *const kUmbrellaUserDefPlacesStore = @"kUmbrellaUserDefPlacesStore";
NSString *const kUmbrellaUserDefHelpShowed = @"kUmbrellaUserDefHelpShowed";
NSString *const kUmbrellaUserDefUnitSettings = @"kUmbrellaUserDefUnitSettings";

+ (instancetype)sharedInstance {
    static UMUserData *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[UMUserData alloc] init];
    });
    return sharedInstance;
}

- (void) getLocalData {
    self.userPlaces = [NSMutableArray arrayWithArray: [[NSUserDefaults standardUserDefaults] objectForKey:kUmbrellaUserDefPlacesStore]];
}

- (NSUInteger) numberOfLocalData {
    return self.userPlaces.count;
}

- (void) deletePlaceAtIndex: (NSUInteger) index {
    [self.userPlaces removeObjectAtIndex:index];
    [[NSUserDefaults standardUserDefaults] setObject:self.userPlaces forKey:kUmbrellaUserDefPlacesStore];
}

- (void) addPlaceAtLat: (CGFloat) lat Lon: (CGFloat) lon {
    NSDictionary *newPlace = @{@"lat":[NSNumber numberWithFloat:lat],
                         @"lon":[NSNumber numberWithFloat:lon]};
    [self.userPlaces addObject:newPlace];
    [[NSUserDefaults standardUserDefaults] setObject:self.userPlaces forKey:kUmbrellaUserDefPlacesStore];
}

@end
