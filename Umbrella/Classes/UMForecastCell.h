//
//  UMForecastCell.h
//  Umbrella
//
//  Created by Janusz Bień on 21.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMWeatherData.h"

@interface UMForecastCell : UICollectionViewCell

@property(nonatomic, weak) IBOutlet UILabel *placeDay;
@property(nonatomic, weak) IBOutlet UILabel *placeDesc;
@property(nonatomic, weak) IBOutlet UILabel *placeTemp;
@property(nonatomic, weak) IBOutlet UILabel *placeHuminidy;
@property(nonatomic, weak) IBOutlet UILabel *placeWind;

@property(nonatomic, weak) IBOutlet UIImageView *placeIcon;
@property(nonatomic, strong) UMWeatherData *weatherData;

- (void) fillWithData: (NSDictionary *) data atIndex: (NSUInteger) index;


@end
