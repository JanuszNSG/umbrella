//
//  UMWeatherData.m
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import "UMWeatherData.h"
//http://openweathermap.org/img/w/10d.png

NSString* const kOpenWeatherDomain = @"openweathermap.org";

NSString* const kOpenWeatherData = @"/data/2.5/";
NSString* const kOpenWeatherCurrent = @"weather";
NSString* const kOpenWeatherForecast = @"forecast";

NSString* const kOpenWeatherAPIKey = @"c6e381d8c7ff98f0fee43775817cf6ad";

@interface UMWeatherData()

@property (nonatomic, retain) NSURLRequest *reques;
@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, retain) NSError *error;
@property (nonatomic, retain) NSData *data;

@end

@implementation UMWeatherData

- (instancetype) init {
    if (self = [super init]){
        _reques = nil;
        _response = nil;
        _error = nil;
        _data = nil;
        _weatherData = nil;
    }
    return self;
}

- (NSURL *)URLForIcon: (NSString *) iconName {
    return [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/img/w/%@.png", kOpenWeatherDomain, iconName]];
}

- (NSURL *)URLForLat: (CGFloat) lat Lon: (CGFloat) lon withForecast: (BOOL) forecast {
    NSString *command = @"";
    if (forecast) {
        command = kOpenWeatherForecast;
    } else {
        command = kOpenWeatherCurrent;
    }
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://api.%@%@%@", kOpenWeatherDomain, kOpenWeatherData, command]];
    
    NSURLComponents *components = [[NSURLComponents alloc] initWithURL:url resolvingAgainstBaseURL:NO];
    NSArray *query = @[
                       [NSURLQueryItem queryItemWithName:@"appid" value:kOpenWeatherAPIKey],
                       [NSURLQueryItem queryItemWithName:@"lat" value:[NSString stringWithFormat:@"%f", lat]],
                       [NSURLQueryItem queryItemWithName:@"lon" value:[NSString stringWithFormat:@"%f", lon]],
                       [NSURLQueryItem queryItemWithName:@"units" value:@"metric"]
                       ];
    [components setQueryItems:query];
    return [components URL];
}

- (void) getWeatherForLat: (CGFloat) lat Lon: (CGFloat) lon withForecast: (BOOL) forecast {
    [[[NSURLSession sharedSession] dataTaskWithURL:[self URLForLat: lat Lon:lon withForecast:forecast]
                                 completionHandler:^(NSData *data,
                                    NSURLResponse *response,
                                    NSError *error) {
                if (data) {
                    if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                        NSUInteger statusCode = [(NSHTTPURLResponse *) response statusCode];
                        if (statusCode!= 200) {
                            [UMUtils infoAllertTitled:@"Error" Message:[NSString stringWithFormat:@"Network error: %lu", (unsigned long)statusCode]];
                            return;
                        }
                    } else {
                        [UMUtils infoAllertTitled:@"Error" Message:@"Wrong server response"];
                        return;
                    }
                } else {
                    [UMUtils infoAllertTitled:@"Error" Message:@"No data recieved from server"];
                    return;
                }
                NSError *parseError = nil;
                self.weatherData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                if (!self.weatherData) {
                    [UMUtils infoAllertTitled:@"Error" Message:@"Server data corrupted"];
                    return;
                }
                self.weatherWithForecast = forecast;
                [self.delegate weatherDataAreRedy];
        }] resume];
}

- (void) getCurrentWeatherForLat: (CGFloat) lat Lon: (CGFloat) lon {
    [self getWeatherForLat:lat Lon:lon withForecast:NO];
}

- (void) getForecastedWeatherForLat: (CGFloat) lat Lon: (CGFloat) lon {
    [self getWeatherForLat:lat Lon:lon withForecast:YES];
}

@end
