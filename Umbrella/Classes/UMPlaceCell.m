//
//  UMPlaceCell.m
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import "UMPlaceCell.h"
#import "UMUserData.h"

@implementation UMPlaceCell

- (void) fillWithData: (NSDictionary *) data {
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(prepareToDelete:)];
    longPressGesture.delegate = self;
    longPressGesture.minimumPressDuration = 0.5f;
    longPressGesture.delaysTouchesBegan = YES;
    [self addGestureRecognizer:longPressGesture];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showForecast:)];
    tapGesture.delegate = self;
    tapGesture.delaysTouchesBegan = YES;
    [self addGestureRecognizer:tapGesture];

    self.placeDeleteBtn.hidden = data[@"delete"];
    self.weatherData = [[UMWeatherData alloc] init];
    self.placeBuzyIndicator.hidden = NO;
    [self.placeBuzyIndicator startAnimating];
    self.weatherData.delegate = self;
    [self.weatherData getCurrentWeatherForLat:[data[@"lat"] floatValue] Lon:[data[@"lon"] floatValue]];
    
    UIImage *btnImage = [[UIImage imageNamed:@"back_btn"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.placeDeleteBtn setImage:btnImage forState:UIControlStateNormal];
    self.placeDeleteBtn.tintColor = [UIColor colorWithRed:0.3f green:0.59f blue:0.59f alpha:1.0f];
    
    self.placeDeleteBtn.hidden = YES;
    self.backgroundColor = [UIColor whiteColor];
}

- (void) prepareToDelete: (UILongPressGestureRecognizer *) gestureRecognizer {
    self.placeDeleteBtn.hidden = NO;
    self.backgroundColor = [UIColor colorWithRed:1.0 green:0.82 blue:0.78 alpha:1.0];
}

- (void) showForecast: (UITapGestureRecognizer *) gestureRecognizer {
    self.placeDeleteBtn.hidden = YES;
    self.backgroundColor = [UIColor whiteColor];
    [UMUserData sharedInstance].currentPlace = self.tag;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kShowForecast" object:nil];
}

- (IBAction)deletePlaceBtnPressed:(UIButton *)sender {
    [[UMUserData sharedInstance] deletePlaceAtIndex: self.tag];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kRefreshPlacesList" object:nil];
}

- (void)weatherDataAreRedy {
    NSString *placeIconName = @"";
    NSString *placeName = @"";
    NSString *placeDesc = @"";
    NSString *placeTemp = @"";
    NSString *placeHuminidy = @"";
    NSString *placeWind = @"";
    
    placeName = self.weatherData.weatherData[@"name"];
    if (self.weatherData.weatherData[@"weather"]) {
        placeDesc = self.weatherData.weatherData[@"weather"][0][@"description"];
        placeIconName = self.weatherData.weatherData[@"weather"][0][@"icon"];
    }
    if (self.weatherData.weatherData[@"main"]) {
        placeTemp = [NSString stringWithFormat:@"%0.0f°C", [self.weatherData.weatherData[@"main"][@"temp"] floatValue]];
        placeHuminidy = [NSString stringWithFormat:@"%0.0f%%", [self.weatherData.weatherData[@"main"][@"humidity"] floatValue]];
    }
    if (self.weatherData.weatherData[@"wind"]) {
        placeWind = [NSString stringWithFormat:@"%0.0fm/s", [self.weatherData.weatherData[@"wind"][@"speed"] floatValue]];
    }

    if (placeIconName.length>0) {
        dispatch_async(dispatch_get_global_queue(0,0), ^{
            NSData *imageData = [[NSData alloc] initWithContentsOfURL: [self.weatherData URLForIcon:placeIconName]];
            if ( imageData != nil ) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.placeIcon.image = [UIImage imageWithData: imageData];
                    self.placeIcon.hidden = NO;
                });
            }
        });
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.placeName.text = placeName;
        self.placeDesc.text = placeDesc;
        self.placeTemp.text = placeTemp;
        self.placeHuminidy.text = placeHuminidy;
        self.placeWind.text = placeWind;
        [self.placeBuzyIndicator stopAnimating];
        self.placeBuzyIndicator.hidden = YES;
    });
}


@end
