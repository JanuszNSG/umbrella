//
//  ViewController.h
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UMHelpView.h"
#import "UMMapView.h"
#import "UMPlacesView.h"
#import "UMForecastView.h"
#import "UMPlaceCell.h"

@interface ViewController : UIViewController <UMPlaceCellDelegate>

@property (weak, nonatomic) IBOutlet UMHelpView *helpView;
@property (weak, nonatomic) IBOutlet UMMapView *mapView;
@property (weak, nonatomic) IBOutlet UMPlacesView *placesView;
@property (weak, nonatomic) IBOutlet UMForecastView *forecastView;

@end

