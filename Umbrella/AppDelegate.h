//
//  AppDelegate.h
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

