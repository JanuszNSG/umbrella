//
//  UMUtils.h
//  Umbrella
//
//  Created by Janusz Bień on 21.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UMUtils : NSObject

+ (void) infoAllertTitled: (NSString *) title Message: (NSString *) message;

@end
