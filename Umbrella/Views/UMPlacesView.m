//
//  UMPlacesView.m
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import "UMPlacesView.h"
#import "UMPlaceCell.h"

@implementation UMPlacesView

NSInteger const kPlaceCellWidth = 320;

- (void) awakeFromNib {
    [super awakeFromNib];
    UIImage *btnImage = [[UIImage imageNamed:@"add_btn"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.addPlaceBtn setImage:btnImage forState:UIControlStateNormal];
    self.addPlaceBtn.tintColor = [UIColor colorWithRed:0.3f green:0.59f blue:0.59f alpha:1.0f];
    self.userData = [UMUserData sharedInstance];
    [self.userData getLocalData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSUInteger noOfData = [self.userData numberOfLocalData];
    self.emptyInfoLbl.hidden = (noOfData!=0);
    return noOfData;
}

- (__kindof UMPlaceCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"weatherPlaceCell";
    UMPlaceCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.tag = indexPath.row;
    [cell fillWithData:[self.userData.userPlaces objectAtIndex:indexPath.row]];
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    NSInteger numberOfCells = self.frame.size.width / kPlaceCellWidth;
    NSInteger edgeInsets = (self.frame.size.width - (numberOfCells * kPlaceCellWidth)) / (numberOfCells + 1);
    return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
}

@end
