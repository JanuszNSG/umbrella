//
//  UMForecastView.h
//  Umbrella
//
//  Created by Janusz Bień on 21.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMUserData.h"
#import "UMForecastCell.h"

@interface UMForecastView : UIView <UICollectionViewDelegate, UICollectionViewDataSource, UMWeatherDataDelegate>

@property(nonatomic, weak) IBOutlet UICollectionView *forecastCollection;
@property(nonatomic, weak) IBOutlet UIButton *closeForecastBtn;

@property(nonatomic, weak) IBOutlet UILabel *forecastNameLbl;
@property(nonatomic, weak) IBOutlet UIActivityIndicatorView *forecastBuzyIndicator;

@property (nonatomic, retain) UMWeatherData *weatherData;

@property(readwrite) NSUInteger placeIndex;
@property(readwrite) NSArray* placeForecast;

- (void) loadCuttentPlaceData;

@end
