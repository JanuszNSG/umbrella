//
//  UMHelpView.m
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import "UMHelpView.h"

@implementation UMHelpView

- (void) awakeFromNib {
    [super awakeFromNib];
    NSURL *helpURL = [[NSBundle mainBundle] URLForResource:@"UmbrellaHelp" withExtension:@"html"];
    [self.helpWebView loadRequest:[NSURLRequest requestWithURL:helpURL]];
    
    UIImage *btnImage = [[UIImage imageNamed:@"done_btn"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.helpCloseBtn setImage:btnImage forState:UIControlStateNormal];
    self.helpCloseBtn.tintColor = [UIColor colorWithRed:0.3f green:0.59f blue:0.59f alpha:1.0f];
}

- (IBAction)hideHelp:(UIButton *)sender {
    self.hidden = YES;
}

- (IBAction)showHelp:(UIButton *)sender {
    self.hidden = NO;
}

@end
