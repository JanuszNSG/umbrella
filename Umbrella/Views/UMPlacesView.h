//
//  UMPlacesView.h
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UMUserData.h"

@interface UMPlacesView : UIView <UICollectionViewDelegate, UICollectionViewDataSource>

@property(nonatomic, weak) IBOutlet UICollectionView *placesCollection;
@property(nonatomic, weak) IBOutlet UIButton *addPlaceBtn;
@property(nonatomic, weak) IBOutlet UILabel *emptyInfoLbl;

@property(nonatomic, weak) UMUserData *userData;
@end
