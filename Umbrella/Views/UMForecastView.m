//
//  UMForecastView.m
//  Umbrella
//
//  Created by Janusz Bień on 21.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import "UMForecastView.h"

@implementation UMForecastView

NSInteger const kForecasrCellWidth = 320;

- (void) awakeFromNib {
    [super awakeFromNib];
    UIImage *btnImage = [[UIImage imageNamed:@"done_btn"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.closeForecastBtn setImage:btnImage forState:UIControlStateNormal];
    self.closeForecastBtn.tintColor = [UIColor colorWithRed:0.3f green:0.59f blue:0.59f alpha:1.0f];
    
    self.forecastCollection.hidden = YES;
    self.forecastNameLbl.text = @"";
    self.forecastBuzyIndicator.hidden = NO;
    [self.forecastBuzyIndicator startAnimating];
    
    self.placeForecast = nil;
}

- (void) loadCuttentPlaceData {
    UMUserData *userData = [UMUserData sharedInstance];
    NSUInteger currentIndex = [userData currentPlace];
    NSDictionary *data = [userData.userPlaces objectAtIndex:currentIndex];
    self.weatherData = [[UMWeatherData alloc] init];
    self.weatherData.delegate = self;
    [self.weatherData getForecastedWeatherForLat:[data[@"lat"] floatValue] Lon:[data[@"lon"] floatValue]];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return (self.placeForecast.count>=5) ? 5 : self.placeForecast.count;
}

- (__kindof UMForecastCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"UMForecastCell";
    UMForecastCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    [cell fillWithData:[self.placeForecast objectAtIndex:indexPath.row] atIndex: indexPath.row];
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    NSInteger numberOfCells = self.frame.size.width / kForecasrCellWidth;
    NSInteger edgeInsets = (self.frame.size.width - (numberOfCells * kForecasrCellWidth)) / (numberOfCells + 1);
    return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
}

- (void)weatherDataAreRedy {
    NSString *placeName = @"";
    if (self.weatherData.weatherData[@"city"]) {
        placeName = self.weatherData.weatherData[@"city"][@"name"];
    }
    self.placeForecast = self.weatherData.weatherData[@"list"];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.forecastBuzyIndicator stopAnimating];
        self.forecastBuzyIndicator.hidden = YES;
        self.forecastNameLbl.text = placeName;
        self.forecastCollection.hidden = NO;
        [self.forecastCollection reloadData];
    });

}

@end
