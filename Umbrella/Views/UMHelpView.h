//
//  UMHelpView.h
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface UMHelpView : UIView

@property(nonatomic, weak) IBOutlet UIWebView *helpWebView;
@property(nonatomic, weak) IBOutlet UIButton *helpCloseBtn;

@end
