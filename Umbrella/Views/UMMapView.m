//
//  UMMapView.m
//  Umbrella
//
//  Created by Janusz Bień on 20.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import "UMMapView.h"
#import "UMUserData.h"

@implementation UMMapView

- (void) awakeFromNib {
    [super awakeFromNib];
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(addPlaceBasedOnLocation:)];
    longPress.delegate = self;
    longPress.minimumPressDuration = 0.3f;
    longPress.delaysTouchesBegan = YES;
    [self.mapView addGestureRecognizer:longPress];
    
    UIImage *btnImage = [[UIImage imageNamed:@"back_btn"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.mapCloseBtn setImage:btnImage forState:UIControlStateNormal];
    self.mapCloseBtn.tintColor = [UIColor colorWithRed:0.3f green:0.59f blue:0.59f alpha:1.0f];
}

- (void) addPlaceBasedOnLocation: (UILongPressGestureRecognizer *) gestureRecognizer {
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan) {
        return;
    }
    CGPoint touchAt = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D location = [self.mapView convertPoint:touchAt toCoordinateFromView:self.mapView];
    [[UMUserData sharedInstance] addPlaceAtLat:location.latitude Lon:location.longitude];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kRefreshPlacesList" object:nil];
}


@end
