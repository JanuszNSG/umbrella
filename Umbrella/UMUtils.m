//
//  UMUtils.m
//  Umbrella
//
//  Created by Janusz Bień on 21.05.2017.
//  Copyright © 2017 NSG. All rights reserved.
//

#import "UMUtils.h"

@implementation UMUtils

+ (void) infoAllertTitled: (NSString *) title Message: (NSString *) message {
    UIAlertController *alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action) {
                               }];
    [alert addAction:okButton];
    [[[[UIApplication sharedApplication] keyWindow] rootViewController]
     presentViewController:alert animated:YES completion:^{}];
}


@end
